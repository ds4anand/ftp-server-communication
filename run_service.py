import sqlite3
import re
import time
import subprocess
from datetime import datetime

def check_time_format(input_str):
    pattern = r'^([01]\d|2[0-3]):[0-5]\d$'
    return re.match(pattern, input_str) is not None

def run_schedule(job_type, server_ID, local_path, job_frequency, schedule_time, wait_period, unique_id, project_file):
    result = subprocess.run(['python', 'schedule_run.py', job_type, server_ID, local_path, job_frequency, schedule_time, wait_period, unique_id, project_file])
    return result.returncode    

def main():
    # Establish a connection to the SQLite database
    connection = sqlite3.connect(r'D:\Git_Codes\file_transfer_ftp_server\sqlite_db\ftp_communication.db')
    cursor = connection.cursor()

    while True:
        # Fetch data from the SQLite table
        cursor.execute("SELECT Server_ID, Job_type, Unique_id, Waiting_period, Scheduled_time, Job_frequency, Project_Path, Local_path, Job_status FROM scheduled_jobs")
        rows = cursor.fetchall()
        for row in rows:
            server_ID, job_type, unique_id, wait_period, schedule_time, job_frequency, project_file, local_path, job_status = row
            server_ID = str(server_ID)
            current_time = datetime.now().strftime("%H:%M")
            if current_time == schedule_time:
                if check_time_format(schedule_time):
                    if job_status not in ['1','3']:
                        if job_frequency == '1':
                            print('run once:', schedule_time)
                            run_schedule(job_type, server_ID, local_path, job_frequency, schedule_time, wait_period, unique_id, project_file)
                            time.sleep(60)
                            break
                        else:
                            print('run daily:', schedule_time)
                            run_schedule(job_type, server_ID, local_path, job_frequency, schedule_time, wait_period, unique_id, project_file)
                            time.sleep(60)
                            break
            else:
                if schedule_time.isnumeric():
                    print('sec:', schedule_time)
                    if job_status not in ['3']:
                        while True:
                            run_schedule(job_type, server_ID, local_path, job_frequency, schedule_time, wait_period, unique_id, project_file)
                            time.sleep(int(schedule_time))
        

if __name__ == "__main__":
    main()
