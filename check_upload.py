from cryptography.fernet import Fernet
import os, sys
from ftplib import FTP
import sqlite3
import time
import csv


def get_credentials_by_key(all_servers, server_ID):
    for server in all_servers:
        server_no, key, server, port, encrypted_username, encrypted_password= server
        if server_no == server_ID:
            # print("Credentials found")
            fernet = Fernet(key)
            username = fernet.decrypt(encrypted_username.encode()).decode()
            password = fernet.decrypt(encrypted_password.encode()).decode()
            return server, port, username, password
    return None


def upload(ftp, new_files, file_path):
    if new_files:
        # Download new files
        for file_name in new_files:
            # print('file: ',file_name)
            file = os.path.join(file_path, file_name)
            with open(file, 'rb') as local_file:
                ftp.storbinary(f"STOR {file_name}", local_file)
            print(f"New Found {file_name}")
        print("New files uploaded successfully at", time.strftime("%Y-%m-%d %H:%M:%S"))   
        
    else:
        print("No new files to upload.")


def upload_files(credentials, local_path, remote_path): 
    try:
        server, port, username, password = credentials 
        # Connect to the FTP server
        ftp = FTP()
        ftp.connect(host=server, port=int(port))
        ftp.login(username, password)
        ftp.sendcmd('PASV')

        ftp.cwd(remote_path)       
        current_path = ftp.pwd()
        # print('cur_path-',current_path)
        foldername = current_path.split('/')[-1]
        # print(foldername)

        # List the files in the remote folder
        remote_files = ftp.nlst()    
        # print('remote_files: ',remote_files)
        
        main_folder_path = os.path.join(local_path, foldername)
        check_path =r'D:\Git_Codes\file_transfer_ftp_server\check_existing'
        check_folder = os.path.join(check_path, foldername)
        for path in [main_folder_path, check_folder]:
            if not os.path.exists(path):
                os.makedirs(path)
                
        for item in remote_files:
            if '.' in item:
                # List the files in the local folder
                local_files = os.listdir(main_folder_path)
                new_files =  [x for x in local_files if x not in remote_files]
                upload(ftp, new_files, main_folder_path)
                break
            else:
                inside_folder_path = current_path+'/'+item+'/'
                ftp.cwd(inside_folder_path)
                remote_folder_path = ftp.pwd()
                remote_files = ftp.nlst()
                # sub-folder creation in local
                subfolder_path = os.path.join(main_folder_path, item)
                local_files = os.listdir(subfolder_path)
                # print('files: ',local_files)
                # New files on the remote server
                new_files =  [x for x in local_files if x not in remote_files]
                upload(ftp, new_files, subfolder_path)
                break
        
        # Close the FTP connection   
        ftp.quit()
    except ConnectionError or ConnectionRefusedError or ConnectionAbortedError or ConnectionResetError as e:
        print(e)

if __name__ == '__main__':
    server_ID = int(sys.argv[1])
    local_path = sys.argv[2]
    uid = sys.argv[3]
    project_file = sys.argv[4]
    # server_ID = int(input('Enter server no'))
    # local_path = input('Enter local path')
    # project_file = input('For which project you want to upload')
    conn = sqlite3.connect(r"sqlite_db\ftp_communication.db")
    cursor = conn.cursor()
    cursor.execute("SELECT Server_ID, Encription_key, Server_ip, Port, encrypted_username, encrypted_password FROM server_credential")
    all_servers = cursor.fetchall()
    credentials = get_credentials_by_key(all_servers, server_ID)
    if credentials is not None:
        load_project = open(f'D:\\Git_Codes\\file_transfer_ftp_server\\projects_remote_paths\\{project_file}.txt', 'r')
        for path in load_project:
            remote_path = rf"{path.strip()}"
            result = upload_files(credentials, local_path, remote_path)
        running_folder = r"D:\Git_Codes\file_transfer_ftp_server\logs\running_logs"
        os.remove(running_folder+'\\'+f'{uid}.txt')
    else:
        print("Credentials not found for the given key.")
