import uuid
import sqlite3
from datetime import datetime

def get_interval(interval_type, interval_value):
    if interval_type == "h":
        minutes = int(input(f"Enter the number of minutes (0 to 59): "))
        # seconds = int(input(f"Enter the number of seconds (0 to 59): "))
        return interval_value * 3600 + minutes * 60 
    elif interval_type == "m":
        # seconds = int(input(f"Enter the number of seconds (0 to 59): "))
        return interval_value * 60
    else:
        return interval_value
    
def waiting_period():
    interval_type = input("you want minute or seconds(M/S): ").lower()
    if interval_type in ["m", "s"]:
        interval_value = int(input(f"Enter the time in {interval_type} (0 to 59): "))
    if interval_type == "m":
        seconds = int(input(f"Enter the number of seconds (0 to 59): "))
        return interval_value * 60 + seconds
    else:
        return interval_value
    
def time_schedule(choice):
    while True:
        if choice == "1":
            spec_time = input("Enter the specific time for Job in 24-hour format(HH:MM): ")
            try:
                at_time = datetime.strptime(spec_time,"%H:%M")
                specific_time = str(at_time)[11:16]
                return specific_time
            except ValueError:
                return "Invalid time format. Use HH:MM:SS"

        elif choice == "2":
            interval_type = input("Enter the time type (H/M): ").lower()
            if interval_type in ["h", "m"]:
                interval_value = int(input(f"Enter the time in {interval_type}: "))
                interval_seconds = get_interval(interval_type, interval_value)
                return interval_seconds
            else:
                print("Invalid interval type. Use 'hours', 'minutes', or 'seconds'.")
        else:
            break

if __name__ == '__main__':
    taskk = input("Please enter your operation(Upload/Download): ")
    server_ID = input("Enter Server_ID for this task: ")
    print("You can Schedule your task at any perticular time or any time interval.\nfor any perticular time - enter '1'\nfor any time interval - enter '2'")
    choice_t = input("Enter your choice: ")
    sched_time = time_schedule(choice_t)
    if choice_t == '2':
        freq = 'n'
    else:
        freq = input("How many times you want to perform this job: ")
    choice = input('do you want to set waiting period (Y/N): ')
    if choice.lower() == 'y':
        wait_period = waiting_period()
        if wait_period <= 0:
            wait_period = '.01'
    else:
        wait_period = '.01'
    
    project_name = input(r"Name of the project: ")
    local_path = input(r"Local Path of download or Upload: ")
    job_status = 0
    # Generate a unique ID for this task
    unique_id = str(uuid.uuid4()) 
    # matching_id = unique_id + '_m' # Generate a random UUID

    conn = sqlite3.connect(r"sqlite_db\ftp_communication.db")
    cursor = conn.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS scheduled_jobs (
                   Server_ID INTEGER,
                   Job_type TEXT,
                   Unique_id TEXT,
                   Job_frequency TEXT,
                   Scheduled_time TEXT,
                   Waiting_period TEXT,
                   Project TEXT,
                   Local_path TEXT,
                   Job_status TEXT )''')
    
    cursor.execute("INSERT INTO scheduled_jobs (Server_ID, Job_type, Unique_id, Job_frequency, Scheduled_time, Waiting_period, Project, Local_path, Job_status) VALUES (?,?,?,?,?,?,?,?,?)", (server_ID, taskk, unique_id, freq, sched_time, wait_period, project_name, local_path, job_status))
    conn.commit()
    conn.close()