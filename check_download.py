from cryptography.fernet import Fernet
import os, sys
from ftplib import FTP
import sqlite3
import time

def get_credentials_by_key(all_servers, server_ID):
    for server in all_servers:
        server_no, key, server, port, encrypted_username, encrypted_password= server
        if server_no == server_ID:
            # print("Credentials found")
            fernet = Fernet(key)
            username = fernet.decrypt(encrypted_username.encode()).decode()
            password = fernet.decrypt(encrypted_password.encode()).decode()
            # print(server, port, username, password)
            return server, port, username, password
    return None

def is_ftp_file_or_folder(ftp, path):
    try:
        # ftp.cwd('cd')
        # print(path)
        ftp.cwd(path)  # Attempt to change to the given path
        ftp.cwd('cd')
        return "folder"
    except Exception as e:
        if "550" in str(e):
            return "file"
        else:
            return "error"

def download(ftp, new_files, file_path, check_path, folder):
    if new_files:
        # Download new files
        for file_name in new_files:
            # print('file: ',file_name)
            file = os.path.join(file_path, file_name)
            with open(file, 'wb') as local_file:
                ftp.retrbinary(f"RETR {file_name}", local_file.write)
            with open(f"{check_path}\\{folder}.log", "a") as log_file:
                log_file.write(file_name + "\n")
            print(f"New Found {file_name}")
        print("New files downloaded successfully at", time.strftime("%Y-%m-%d %H:%M:%S"))   
    else:
        print("No new files to download.")
    

def download_files(credentials, local_path, remote_path): 
    try:
        server, port, username, password = credentials 
        # Connect to the FTP server
        ftp = FTP()
        ftp.connect(host=server, port=int(port))
        ftp.login(username, password)
        ftp.sendcmd('PASV')

        ftp.cwd(remote_path)       
        current_path = ftp.pwd()
        # print('cur_path-',current_path)
        foldername = current_path.split('/')[-1]
        # print(foldername)

        # List the files in the remote folder
        remote_files = ftp.nlst()    
        print('remote_files: ',remote_files)

        main_folder_path = os.path.join(local_path, foldername)
        check_path =r'D:\Git_Codes\file_transfer_ftp_server\check_existing'
        check_folder = os.path.join(check_path, foldername)
        for path in [main_folder_path, check_folder]:
            if not os.path.exists(path):
                os.makedirs(path)
        # print('main_folder_path: ', main_folder_path)

        for item in remote_files:
            if '.' in item:
                # List the files in the local folder
                local_files = []
                with open(f"{check_folder}\\{foldername}.log", "w") as log_file:
                    pass
                with open(f"{check_folder}\\{foldername}.log", "r") as log_file:
                    for line in log_file:
                        # Split the line into items using the specified delimiter
                        items = line.strip().split(',')
                        local_files.extend(items)
                print('files: ',local_files)
                new_files =  [x for x in remote_files if x not in local_files]
                download(ftp, new_files, main_folder_path, check_folder, foldername)
            else:
                inside_folder_path = current_path+'/'+item+'/'
                ftp.cwd(inside_folder_path)
                remote_folder_path = ftp.pwd()
                remote_files = ftp.nlst()
                # sub-folder creation in local
                subfolder_path = os.path.join(main_folder_path, item)
                check_subfolder_path = os.path.join(check_path, foldername)
                for path in [subfolder_path]:
                    if not os.path.exists(path):
                        os.makedirs(path)
                local_files = []
                with open(f"{check_subfolder_path}\\{item}.log", "w") as log_file:
                    pass
                with open(f"{check_subfolder_path}\\{item}.log", "r") as log_file:
                    for line in log_file:
                        # Split the line into items using the specified delimiter
                        items = line.strip().split(',')
                        local_files.extend(items)
                new_files =  [x for x in remote_files if x not in local_files]
                download(ftp, new_files, subfolder_path, check_subfolder_path, item)
        # Close the FTP connection   
        ftp.quit()
    except ConnectionError or ConnectionRefusedError or ConnectionAbortedError or ConnectionResetError as e:
        print(e)

if __name__ == '__main__':
    # server_ID = int(sys.argv[1])
    # local_path = sys.argv[2]
    # uid = sys.argv[3]
    # project_file = sys.argv[4]
    server_ID = 1
    local_path = r'D:\Git_Codes\file_transfer_ftp_server\output'
    project_file = input('For which project you want to download: ')
    # print(type(local_path), type(server_ID))
    conn = sqlite3.connect(r"sqlite_db\ftp_communication.db")
    cursor = conn.cursor()
    cursor.execute("SELECT Server_ID, Encription_key, Server_ip, Port, encrypted_username, encrypted_password FROM server_credential")
    all_servers = cursor.fetchall()
    credentials = get_credentials_by_key(all_servers, server_ID)
    if credentials is not None:
        load_project = open(f'D:\\Git_Codes\\file_transfer_ftp_server\\projects_remote_paths\\{project_file}.txt', 'r')
        for path in load_project:
            remote_path = rf"{path.strip()}"
            result = download_files(credentials, local_path, remote_path)
        running_folder = r"D:\Git_Codes\file_transfer_ftp_server\logs\running_logs"
        # os.remove(f'{running_folder}\{uid}.txt')
    else:
        print("Credentials not found for the given key.")
