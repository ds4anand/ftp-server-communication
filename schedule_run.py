import csv
import os, sys, shutil
import time
import subprocess
import pandas as pd
import numpy as np
import sqlite3
import uuid
from datetime import datetime
running_folder = r"D:\Git_Codes\file_transfer_ftp_server\logs\running_logs"
completed_folder = r"D:\Git_Codes\file_transfer_ftp_server\logs\completed_logs"
error_folder = r"D:\Git_Codes\file_transfer_ftp_server\logs\error_logs"
for folder in [running_folder,completed_folder,error_folder]:
    if not os.path.exists(folder):
        os.makedirs(folder)

def running_jobs(msg, uid):
    with open(running_folder+'\\'+f'{uid}.txt', 'w+') as running:
        running.write(msg)
        running.close()

def completed_logs(server_ID, process_id, job_type, stdout, uid):
    # Move log file to the completed_jobs folder
     with open(completed_folder+'\\'+f'{str(uid)}.txt', 'w+') as running:
        msg = f'Server ID: {server_ID},\nProcessed: {job_type},\nProccess ID: {process_id},\nmsg: {str(stdout)}'
        running.write(msg)
        running.close()

def error_logs(server_ID, process_id, job_type, stderr, uid):
    with open(error_folder+'\\'+f'{str(uid)}.txt', 'w+') as error:
        msg = f'Server ID: {server_ID},\nProcessed: {job_type},\nProccess ID: {process_id}\nmsg: {str(stderr)}'
        error.write(msg)
        error.close()

def run_upload(server_no, local_path, waiting_period, uid, project_file):
    start = time.time()
    while float(time.time()-start) < float(waiting_period):          
        upload = subprocess.Popen(['python', 'check_upload.py', server_no, local_path, uid, project_file],stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        # Wait for the process to complete
        stdout, stderr = upload.communicate()
        result = upload.returncode
        process_id = upload.pid
        if 'successfully' in str(stdout):
            result = 0
            print("File upload Successful")
            break 
        else:
            print('Again Attempting')
            time.sleep(1)    
    else:
        print(f"We looked for {waiting_period} but new files not found")
    return result, process_id, stdout, stderr

def run_download(server_no, local_path, waiting_period, uid, project_file):
    print('downloading')
    start = time.time()
    while float(time.time()-start) < float(waiting_period):
        # print(type(server_no))          
        download = subprocess.Popen(['python', 'check_download.py', server_no, local_path, uid, project_file],stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        # Wait for the process to complete
        stdout, stderr = download.communicate()
        result = download.returncode
        process_id = download.pid
        if 'successfully' in str(stdout):
            result = 0
            print("File download Successful")
            break 
        else:
            print('Again Attempting')
            time.sleep(1)    
    else:
        print(f"We looked for {waiting_period} sec but new files not found")
    return result, process_id, stdout, stderr

if __name__ == '__main__':
    job_type = sys.argv[1]
    server_ID = sys.argv[2]
    local_path = sys.argv[3]
    job_frequency = sys.argv[4]
    schedule_time = sys.argv[5]
    wait_period = sys.argv[6]
    unique = sys.argv[7]
    project_file = sys.argv[8]
    uid = str(uuid.uuid4())
    
    conn = sqlite3.connect(r"sqlite_db\ftp_communication.db")
    cursor = conn.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS jobs_status (
                   Server_ID INTEGER,
                   Job_type TEXT,
                   Job_id TEXT,
                   Process_id TEXT,
                   Job_start_time TEXT,
                   Job_end_time TEXT,
                   Time_taken TEXT,
                   Job_status TEXT )''')
    
    # cursor.execute("SELECT Server_ID, Job_type, Unique_id, Waiting_period, Scheduled_time, Job_frequency, Local_path FROM scheduled_jobs")
    # rows = cursor.fetchall()
    start_time = time.time()
    job_start_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(start_time))
    msg1 = f'Server ID: {server_ID} running: {job_type}'
    running_jobs(msg1, uid)
    if job_type.lower() == "upload":
        result, process_id, stdout, stderr = run_upload(server_ID, local_path, wait_period, uid, project_file)
    else:
        if job_type.lower() == "download":
            print("----------------")
            result, process_id, stdout, stderr = run_download(server_ID, local_path, wait_period, uid, project_file)  
    end_time = time.time()
    job_end_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(end_time))
    # msg2 = f'Server ID: {server_ID} with unique id: {uid} completed: {job_type}'
    # running_jobs(msg2, uid)
    if result == 0:
        # Move log to completed_jobs folder
        completed_logs(server_ID, process_id, job_type, stdout, uid)
        if job_frequency == '1':
            job_status = '1'
        else:
            job_status = '2'
    else:
        print(str(stderr))
        if len(str(stderr))> 1:
            error_logs(server_ID, process_id, job_type, stderr, uid)
            job_status = '3'
    # Calculate hours, minutes, and seconds
    total_time_taken = end_time-start_time
    # print('total_download_time: ',total_time_taken)
    hours, remainder = divmod(total_time_taken, 3600)
    minutes, remainder = divmod(remainder, 60)
    seconds, milliseconds = divmod(remainder, 1)
    # Format the time difference
    time_difference = f"{int(hours):02d}:{int(minutes):02d}:{int(seconds):02d}:{int(milliseconds*1000):06d}"
    # print("Time Taken (hh:mm:ss:ms):", time_difference)

    cursor.execute("UPDATE scheduled_jobs SET Job_status = ? WHERE Unique_id = ?", (job_status, unique))
    cursor.execute("INSERT INTO jobs_status (Server_ID, Job_type, Job_id, Process_id, Job_start_time, Job_end_time, Time_taken, Job_status) VALUES (?,?,?,?,?,?,?,?)", (server_ID, job_type, uid, process_id, job_start_time, job_end_time, time_difference, job_status))
    conn.commit()
