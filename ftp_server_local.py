from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer

# Define FTP server settings
authorizer = DummyAuthorizer()
authorizer.add_user("Anand", "Anand", homedir='/', perm="elradfmw", msg_login= "Login successful.",
    msg_quit= "Goodbye.")

handler = FTPHandler
handler.authorizer = authorizer

# Define and start the FTP server
server = FTPServer(('127.0.0.1',7000), handler)
server.serve_forever()