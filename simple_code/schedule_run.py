import csv
import os, sys
import time
import subprocess
import pandas as pd
import numpy as np

def run_upload(server_no, local_path, waiting_period):
    start = time.time()
    print(start)
    # import pdb; pdb.set_trace()
    while time.time()-start < waiting_period:          
        upload = subprocess.run(['python', 'check_upload.py', server_no, local_path])
        print('status: ',upload.returncode)
        result = upload.returncode
        if result == 0:
            print("File Upload Successful")
            break
        else:
            print('Again Attempting')
        
    else:
        print(f"We looked for {waiting_period} but new files not found")
    
    return result

def run_download(server_no, local_path, waiting_period):
    start = time.time()
    # print(start)
    # import pdb; pdb.set_trace()
    while time.time()-start < waiting_period:          
        download = subprocess.run(['python', 'check_download.py', server_no, local_path])
        print('status: ',download.returncode)
        result = download.returncode
        if result == 0:
            print("File download Successful")
            break 
        else:
            print('Again Attempting')
    else:
        print(f"We looked for {waiting_period} but new files not found")
    return result

if __name__ == '__main__':
    # operation = input('Enter the operation(upload|download): ')
    # server_no = input('Enter the Server Number you want to perform with: ')
    operation = sys.argv[1]
    server_no = sys.argv[2]
    local_path = sys.argv[3]
    job_frequency = sys.argv[4]
    schedule_time = sys.argv[5]
    waiting_period = sys.argv[6]
    
    schedule_writter = open(r"cred_jobs\running_jobs.csv",'a+', newline="\n")
    schedule_csv = csv.writer(schedule_writter)
    if operation.lower() == "upload":
        up_st_time = time.time()
        result = run_upload(server_no, local_path, waiting_period)
        if result == 0:
            if job_frequency == 1:
                job_status = 1
            else:
                job_status = 2
        else:
            job_status = 3
            df = pd.read_csv(r'cred_jobs\jobs.csv', index_col= False)
            df['job_status'] = np.where((df['job_frequency'] == 1) & (df['Scheduled_time'] == schedule_time), 3 , df['job_status'])
            df.to_csv(r'cred_jobs\jobs.csv', index= False)  

        up_end_time = time.time()
        total_up_time = up_end_time - up_st_time
        minute,second = divmod(total_up_time,60)
        data = [f"server_ID:{server_no}", f"job_type:{operation}", f"job_id:{up_end_time}", f"time taken:{int(minute)} n {int(second)}",f"job_status:{job_status}"]
        schedule_csv.writerow(data)

    elif operation.lower() == "download":
        down_st_time = time.time()
        result= run_download(server_no, local_path, int(waiting_period))
        if result == 0:
            if job_frequency == 1:
                job_status = 1
            else:
                job_status = 2
        else:
            job_status = 3
        down_end_time = time.time()
        total_down_time = down_end_time - down_st_time
        minute,second = divmod(total_down_time,60)
        data = [f"server_ID:{server_no}", f"job_type:{operation}", f"job_id:{down_end_time}", f"time taken:{int(minute)} n {int(second)}",f"job_status:{job_status}"]
        schedule_csv.writerow(data)
    else:
        pass
    
    