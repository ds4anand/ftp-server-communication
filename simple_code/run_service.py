import csv
import re
import schedule
import os,sys
import time
import threading
from datetime import timedelta, datetime
import subprocess
import pandas as pd
import numpy as np

# Function to check if the string has the format hh:mm:ss
def check_time_format(input_str):
    pattern = r'^([01]\d|2[0-3]):[0-5]\d$'
    return re.match(pattern, input_str) is not None

def run_schedule(task_p, server_no, local_path, job_frequency, schedule_time, waiting_period):
    result = subprocess.run(['python', 'schedule_run.py', str(task_p), str(server_no), str(local_path), str(job_frequency), str(schedule_time), str(waiting_period)])
    return result.returncode
    
if __name__ == "__main__":
    runnung_logs = r"D:\Git Codes\file_transfer_ftp_server\logs\running_logs"
    error_logs = r"D:\Git Codes\file_transfer_ftp_server\logs\error_logs"
    completed_logs = r"D:\Git Codes\file_transfer_ftp_server\logs\completed_logs"

    # Create the folders if they don't exist
    for folder in [runnung_logs, error_logs, completed_logs]:
        if not os.path.exists(folder):
            os.makedirs(folder)

    while True:
        # sys.path.join('cred_jobs', 'jobs.csv') 
        # os.path.join('cred_jobs', 'jobs.csv')
        # os.curdir()
        df = pd.read_csv(r'cred_jobs\jobs.csv', index_col= False)
        for i, j in df.iterrows():
            # import pdb; pdb.set_trace()
            server_ID, job_type, wait_period, schedule_time, job_frequency, local_path, job_status = j
            if check_time_format(schedule_time):
                if str(datetime.now().strftime("%H:%M")) == schedule_time: 
                    print('exact_time: ', schedule_time)
                    if (df['job_status'] != '1') or (df['job_status'] != '3'):
                        result = run_schedule(job_type, server_ID, fr'{local_path}', job_frequency, schedule_time, wait_period)
                        # while result == 1:
                        #     time.sleep(1)
                        if result == 0:
                            if int(job_frequency) == 1:
                                df['job_status'] = np.where((df['job_frequency'] == '1') & (df['Scheduled_time'] == schedule_time), '1' , df['job_status'])                
                            else:
                                df['job_status'] = np.where((df['job_frequency'] != '1') & (df['Scheduled_time'] == schedule_time), '2' , df['job_status'])
            else:
                # if not check_time_format(schedule_time):
                    print('sec:',schedule_time)
                    while True:
                        run_schedule(job_type, server_ID, fr'{local_path}', job_frequency, schedule_time, wait_period)
                        df['job_status'] = np.where((df['job_frequency'] != '1') & (df['Scheduled_time'] == schedule_time), '2' , df['job_status'])
                        time.sleep(int(schedule_time))
            
            df.to_csv(r'cred_jobs\jobs.csv', index= False)


            
        
                    
