from cryptography.fernet import Fernet
import configparser
import csv, os
import pandas as pd
    

def write_server_cred(username, password, server, port):
    # Generate or load the encryption key
    key = Fernet.generate_key()    # Generates a key
    # print(type(key))
    key = str(key, encoding='utf-8')
    # print(type(key))

    # Initialize Fernet with the key
    fernet = Fernet(key)
    # Encrypt the credentials
    encrypted_username = fernet.encrypt(username.encode()).decode()
    encrypted_password = fernet.encrypt(password.encode()).decode()

    # field_names = ['key','Server_Number','server', 'port', 'username', 'password', 'remote_folder', 'local_folder']
    data = [(key, server, port, encrypted_username, encrypted_password)]
    csv_file = r"D:\Git Codes\file_transfer_ftp_server\cred_jobs\server_credentials.csv"
    
    # Create a new CSV file
    # with open(csv_file, mode='a+', newline='\n') as file:
    #     writer = csv.writer(file)
    #     try:
    #         with open(csv_file, mode= 'r') as read_file:
    #             reader = csv.reader(read_file)
    #             rows = list(reader)
    #             next_row_no = len(rows) + 1
    #     except FileNotFoundError:
    #         next_row_no = 1
        
    #     for i , row in enumerate(data, start= next_row_no):
    #         writer.writerow([i] + list(row))

    if not os.path.exists(csv_file):
        data = pd.DataFrame(columns = ['Server_ID','Encription_key','Server_ip','Port','encrypted_username', 'encrypted_password'])
        data.to_csv(csv_file, index= False)
    else:
        data = pd.read_csv(csv_file)
    d_len = data.shape[0]
    data.loc[d_len] = [d_len+1, key, server, port, encrypted_username, encrypted_password]
    data.to_csv(csv_file,index= False)


if __name__ == "__main__":
    # Credentials to encrypt
    username = input("Enter User Name: ")
    password = input("Enter Server Password: ")
    server = input("Enter the Server IP Address with you want to communicate: ")
    port = input("Enter the Port No: ")
    # remote_folder = input("Server path to upload or download: ")
    # local_folder = input("Local path to upload or download: ")

    write_server_cred(username, password, server, port)
    print(f"Credentials have been encrypted and saved to server_credentials.csv")
