import os
import csv
import time
from datetime import datetime
import pandas as pd

def get_interval_seconds(interval_type, interval_value):
    if interval_type == "h":
        minutes = int(input(f"Enter the number of minutes (0 to 59): "))
        # seconds = int(input(f"Enter the number of seconds (0 to 59): "))
        return interval_value * 3600 + minutes * 60 
    # elif interval_type == "m":
    #     seconds = int(input(f"Enter the number of seconds (0 to 59): "))
    #     return interval_value * 60 + seconds
    else:
        return interval_value
    
def time_schedule(choice):
    while True:
        if choice == "1":
            spec_time = input("Enter the specific time to schedule the task (HH:MM:SS): ")
            try:
                at_time = datetime.strptime(spec_time,"%H:%M:%S")
                specific_time = str(at_time)[11:]
                return specific_time
            except ValueError:
                return "Invalid time format. Use HH:MM:SS"

        elif choice == "2":
            interval_type = input("Enter the time type (H/M): ").lower()
            if interval_type in ["h", "m"]:
                interval_value = int(input(f"Enter the time in {interval_type}: "))
                interval_seconds = get_interval_seconds(interval_type, interval_value)
                return interval_seconds
            else:
                print("Invalid interval type. Use 'hours', 'minutes', or 'seconds'.")
        else:
            break

if __name__ == '__main__':
    taskk = input("Please enter your operation(Upload/Download): ")
    print("You can Schedule your task at any perticular time or any time interval.\nTo choose perticular time press 1\n or for any time interval press 2")
    choice = input("Enter your choice: ")
    sched_time = time_schedule(choice)
    server_ID = int(input("Enter Server_ID for this task: "))
    freq = input("How many times you want to perform this job (1 or n): ")
    print('if you want a waiting period')
    wait_period = time_schedule("2")
    local_path = input(r"Local Path of download or Upload")
    job_status = 0
    
    csv_file = r"D:\Git Codes\file_transfer_ftp_server\cred_jobs\jobs.csv"
    if not os.path.exists(csv_file):
        data = pd.DataFrame(columns = ['Server_ID','task','wait_period','Scheduled_time','job_frequency','path_local','job_status'])
        data.to_csv(csv_file, index= False)
    else:
        data = pd.read_csv(csv_file)
    d_len = data.shape[0]
    data.loc[d_len] = [server_ID, taskk, wait_period, sched_time, freq, local_path, job_status]
    data.to_csv(csv_file,index= False)

    # data = [op_server, taskk, sched_time]
    # jobs_csv = open("jobs.csv", mode= 'a+', newline="\n")
    # jobb_write = csv.writer(jobs_csv)
    # jobb_write.writerow(data)
