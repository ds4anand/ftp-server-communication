from cryptography.fernet import Fernet
import sqlite3  

def write_server_cred(username, password, server, port, conn):
    # Generate or load the encryption key
    key = Fernet.generate_key()    # Generates a key
    key = str(key, encoding='utf-8')

    # Initialize Fernet with the key
    fernet = Fernet(key)
    # Encrypt the credentials
    encrypted_username = fernet.encrypt(username.encode()).decode()
    encrypted_password = fernet.encrypt(password.encode()).decode()

    cursor = conn.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS server_credential (
                   Server_ID INTEGER PRIMARY KEY,
                   Encription_key TEXT,
                   Server_ip TEXT,
                   Port TEXT,
                   encrypted_username TEXT,
                   encrypted_password TEXT )''')
                   
    
    cursor.execute("SELECT COUNT (*) FROM server_credential WHERE Server_ip = ? AND Port = ?",(server, port))
    existing_row = cursor.fetchone()
    # print(existing_row)
    if existing_row == (0,):
        cursor.execute("SELECT COUNT (*) FROM server_credential")
        table_len = cursor.fetchone()[0]
        cursor.execute("INSERT INTO server_credential (Server_ID, Encription_key, Server_ip, Port, encrypted_username, encrypted_password) VALUES (?,?,?,?,?,?)", (table_len+1, key, server, port, encrypted_username, encrypted_password))
    else:
        print(f"Port no {port} is already exist with server {server}")
    conn.commit()
    conn.close()

if __name__ == "__main__":
    # Credentials to encrypt
    username = input("Enter User Name: ")
    password = input("Enter Server Password: ")
    server = input("Enter the Server IP Address with you want to communicate: ")
    port = (input("Enter the Port No: "))
    # remote_folder = input("Server path to upload or download: ")
    # local_folder = input("Local path to upload or download: ")
    conn = sqlite3.connect(r"sqlite_db\ftp_communication.db")
    write_server_cred(username, password, server, str(port), conn)
    print(f"Credentials for {server}:{port} have been encrypted and saved.")
